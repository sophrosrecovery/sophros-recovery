Sophros Recovery center helps residents of Jacksonville including our nations veterans and first responders recover from substance use disorders (SUD) and co-occurring mental health issues. Our focus is on client results, and we give each of our clients the best chance at long-term recovery.

Address: 2511 St Johns Bluff Road S, Suite 106, Jacksonville, FL 32246, USA

Phone: 904-447-8979

Website: https://www.sophrosrecovery.com/
